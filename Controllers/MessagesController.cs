﻿using daks_bot.Models.Commands;
using Microsoft.AspNetCore.Mvc;
using Telegram.Bot.Types;

namespace daks_bot.Controllers;

[ApiController]
[Route("[controller]")]
public class MessagesController : ControllerBase
{
    private readonly IEnumerable<ICommand> _commands;

    public MessagesController(IEnumerable<ICommand> commands)
    {
        _commands = commands;
    }

    [Route("update")]
    [HttpPost]
    public async Task<IActionResult> Update([FromBody] Update update)
    {
        var message = update.Message;
        if (message is null)
            return BadRequest();

        foreach (var command in _commands)
        {
            if (!command.Contains(message))
                continue;
            await command.Execute(message);
            break;
        }

        return Ok();
    }
}
