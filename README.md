# daks-bot

daks-bot is a telegram bot that uses Monobank Api to get a list of last transactions from specified card.

## Usage
Set up environment variables:
ApplicationUrl, BotName, BotKey, MonoToken, ClientInfoUrl, StatementUrl, ApprovedChatIds, AccountId, CreditCardNumber

## Deploy to Heroku

```
docker build -t daks-bot .
heroku container:push -a daks-bot web
heroku container:release -a daks-bot web
```