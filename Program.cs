using daks_bot.Models;
using daks_bot.Models.Commands;
using StartupModules;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers().AddNewtonsoftJson();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddHttpClient<ICommand, BalanceCommand>();
builder.Services.AddHttpClient<ICommand, TransactionsCommand>();
builder.Services.AddScoped<ICommand, InfoCommand>();
builder.Services.AddMemoryCache();
builder.Services.AddSingleton<IBotProvider, BotProvider>();
builder.UseStartupModules();

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();
app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();
app.Run();