﻿namespace daks_bot.Models;

public static class AppSettings
{
	public static string ApplicationUrl { get; }
	public static string BotName { get; }
	public static string BotKey { get; }
	public static string MonoToken { get; }
	public static string ClientInfoUrl { get; }
	public static string StatementUrl { get; }
	public static IReadOnlyCollection<string> ApprovedChatIds { get; }
	public static string AccountId { get; }
	public static string CreditCardNumber { get; }

	static AppSettings()
	{
		ApplicationUrl = Environment.GetEnvironmentVariable("ApplicationUrl") ?? string.Empty;
		BotName = Environment.GetEnvironmentVariable("BotName") ?? string.Empty;
		BotKey = Environment.GetEnvironmentVariable("BotKey") ?? string.Empty;
		MonoToken = Environment.GetEnvironmentVariable("MonoToken") ?? string.Empty;
		ClientInfoUrl = Environment.GetEnvironmentVariable("ClientInfoUrl") ?? string.Empty;
		StatementUrl = Environment.GetEnvironmentVariable("StatementUrl") ?? string.Empty;
		ApprovedChatIds = Environment.GetEnvironmentVariable("ApprovedChatIds")?.Split(';') ?? Array.Empty<string>();
		AccountId = Environment.GetEnvironmentVariable("AccountId") ?? string.Empty;
		CreditCardNumber = Environment.GetEnvironmentVariable("CreditCardNumber") ?? string.Empty;
	}
}
