﻿using Telegram.Bot;

namespace daks_bot.Models;

public class BotProvider : IBotProvider
{
    private readonly TelegramBotClient _client;

    public BotProvider()
    {
        _client = new TelegramBotClient(AppSettings.BotKey);
    }

    public async Task SetWebhookAsync()
    {
        var hook = string.Format(AppSettings.ApplicationUrl, "messages/update");
        await _client.SetWebhookAsync(hook);
    }

    public TelegramBotClient Get()
    {
        return _client;
    }
}
