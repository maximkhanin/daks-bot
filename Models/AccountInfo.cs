﻿namespace daks_bot.Models;

public class AccountInfo
{
    public ICollection<Account> Accounts { get; set; } = new List<Account>();
}
