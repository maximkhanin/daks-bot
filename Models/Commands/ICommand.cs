﻿using Telegram.Bot.Types;

namespace daks_bot.Models.Commands;

public interface ICommand
{
    string Name { get; }

    Task<bool> Execute(Message message);

    bool Contains(Message message)
    {
        return message.Text?.Contains(Name) == true && AppSettings.ApprovedChatIds.Contains(message.Chat.Id.ToString());
    }
}