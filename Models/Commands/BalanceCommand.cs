﻿using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace daks_bot.Models.Commands;

public class BalanceCommand : ICommand
{
    public string Name => "/balance";
    private readonly HttpClient _httpClient;
    private readonly IMemoryCache _cache;
    private readonly IBotProvider _provider;

    public BalanceCommand(IMemoryCache cache, HttpClient httpClient, IBotProvider provider)
    {
        _cache = cache;
        _httpClient = httpClient;
        _provider = provider;
    }

    public async Task<bool> Execute(Message message)
    {
        var chatId = message.Chat.Id;
        var messageId = message.MessageId;
        var key = $"{chatId}-balance";

        if (!_cache.TryGetValue(key, out AccountInfo? accountInfo))
        {
            var request = new HttpRequestMessage(HttpMethod.Get, AppSettings.ClientInfoUrl);
            request.Headers.Add("X-Token", AppSettings.MonoToken);

            var response = await _httpClient.SendAsync(request);

            var responseString = await response.Content.ReadAsStringAsync();
            accountInfo = JsonConvert.DeserializeObject<AccountInfo>(responseString);

            if (accountInfo != null)
            {
                _cache.Set(key, accountInfo,
                new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(1)));
            }
        }

        if (accountInfo != null)
        {
            var balance = accountInfo.Accounts.First(a => a.Id == AppSettings.AccountId).Balance;

            await _provider.Get().SendTextMessageAsync(chatId, $"Balance: {balance / (double) 100} UAH", replyToMessageId: messageId);
            return true;
        }

        await _provider.Get().SendTextMessageAsync(chatId, "Please try later", replyToMessageId: messageId);
        return true;
    }
}
