﻿using Telegram.Bot;
using Telegram.Bot.Types;

namespace daks_bot.Models.Commands;

public class InfoCommand : ICommand
{
    private readonly IBotProvider _provider;

    public InfoCommand(IBotProvider provider)
    {
        _provider = provider;
    }

    public string Name => "/info";

    public async Task<bool> Execute(Message message)
    {
        var chatId = message.Chat.Id;
        var messageId = message.MessageId;

        await _provider.Get().SendTextMessageAsync(chatId, $"Credit card number: {AppSettings.CreditCardNumber}", replyToMessageId: messageId);

        return true;
    }
}
