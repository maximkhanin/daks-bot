﻿using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace daks_bot.Models.Commands;

public class TransactionsCommand : ICommand
{
    public string Name => "/transactions";

    private readonly HttpClient _httpClient;
    private readonly IMemoryCache _cache;
    private readonly IBotProvider _provider;

    public TransactionsCommand(IMemoryCache cache, HttpClient httpClient, IBotProvider provider)
    {
        _cache = cache;
        _httpClient = httpClient;
        _provider = provider;
    }

    public async Task<bool> Execute(Message message)
    {
        var chatId = message.Chat.Id;
        var messageId = message.MessageId;
        var key = $"{chatId}-transactions";

        if (!_cache.TryGetValue(key, out ICollection<StatementItem>? statementItems))
        {
            var date = DateTime.Now.Date.AddDays(-15);
            var unixTime = ((DateTimeOffset)date).ToUnixTimeSeconds();
            var request = new HttpRequestMessage(HttpMethod.Get, $"{AppSettings.StatementUrl}{unixTime}");
            request.Headers.Add("X-Token", AppSettings.MonoToken);

            var response = await _httpClient.SendAsync(request);

            var responseString = await response.Content.ReadAsStringAsync();
            statementItems = JsonConvert.DeserializeObject<ICollection<StatementItem>>(responseString);

            if (statementItems != null)
            {
                _cache.Set(key, statementItems, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(1)));
            }
        }

        if (statementItems != null)
        {
            var items = statementItems.ToList().Take(10);
            var messageText = string.Empty;

            foreach (var item in items)
            {
                messageText += $"Amount: {item.Amount / (double)100} UAH;";
                if (item.CashbackAmount != default)
                {
                    messageText += $" Cashback: {item.CashbackAmount / (double)100} UAH;";
                }
                messageText += $" {item.Description};\n";
            }

            if (string.IsNullOrWhiteSpace(messageText))
            {
                await _provider.Get().SendTextMessageAsync(chatId, "No transactions in the last 15 days", parseMode: Telegram.Bot.Types.Enums.ParseMode.Html, replyToMessageId: messageId);
                return true;
            }

            await _provider.Get().SendTextMessageAsync(chatId, messageText, parseMode: Telegram.Bot.Types.Enums.ParseMode.Html, replyToMessageId: messageId);
            return true;
        }

        await _provider.Get().SendTextMessageAsync(chatId, "Please try later", replyToMessageId: messageId);
        return true;
    }
}
