﻿using StartupModules;

namespace daks_bot.Models;

public class TelegramWebhookInitializer : IApplicationInitializer
{
    private readonly IBotProvider _provider;

    public TelegramWebhookInitializer(IBotProvider provider)
    {
        _provider = provider;
    }

    public async Task Invoke()
    {
        await _provider.SetWebhookAsync();
    }
}