﻿using Telegram.Bot;

namespace daks_bot.Models;

public interface IBotProvider
{
    TelegramBotClient Get();
    Task SetWebhookAsync();
}