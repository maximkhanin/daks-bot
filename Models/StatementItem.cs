﻿namespace daks_bot.Models;

public class StatementItem
{
    public string Description { get; set; } = string.Empty;
    public int Amount { get; set; }
    public int CashbackAmount { get; set; }
}
